year();  

// Change year field to this year 
function year() {
  var time = document.getElementById('year');
  time.value = new Date().getFullYear();
}

// ------------------------Send Data-----------------------------
function createCORSRequest(method, url) {
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {
    // XHR for Chrome/Firefox/Opera/Safari.
    xhr.open(method, url, true);
  } else if (typeof XDomainRequest != "undefined") {
    // XDomainRequest for IE.
    xhr = new XDomainRequest();
    xhr.open(method, url);
  } else {
    // CORS not supported.
    xhr = null;
  }

  return xhr;
}

function makeCorsRequest() {
  var url = "http://kuulo.lamk.fi/API/workhours/sum";
  var xhr = createCORSRequest('POST', url);
  if (!xhr) {
    alert('CORS not supported');
    return;
  }

  xhr.onreadystatechange = function() {
    console.log("test");
      if (this.readyState == 4 && this.status == 200) {
          console.log(this.responseText);
          CreateTable(this.responseText);
      }
  };

  // Response handlers.
  xhr.onload = function() {
    console.log("xhr onload function");
    var text = xhr.status; // gets status from server
    if (text == '200') {
      console.log("Viesti lähetetty");
    } else {
      alert('Viestin lähetys epäonnistui');
    }
  };

  xhr.onerror = function() {
    alert('Woops, there was an error making the request.');
  };

  var data = {
      username:localStorage.getItem("userkey"),
      pw: localStorage.getItem("passkey"),
      vuosi:  parseInt(document.querySelector('#year').value)
  };

  var json = JSON.stringify(data);
  console.log(json);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(json);
}
// ------------------------End of send data------------------------

function CreateTable(Data) {
  var obj = JSON.parse(Data);;
  console.log(obj);

  var node = document.getElementById("table");
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }

  for (i in obj) {
    var tr = document.createElement("tr");
    var th = document.createElement("th");
    var td0 = document.createElement("td");
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");
    var td3 = document.createElement("td");

    th.innerHTML =  obj[i].kaynnin_tyyppi;
    td0.innerHTML =  obj[i].tunnit_summa;
    td1.innerHTML =  obj[i].asiakas_summa;
    td2.innerHTML =  obj[i].yli65v_summa;
    td3.innerHTML =  obj[i].alle65v_summa;

    tr.appendChild(th);
    tr.appendChild(td0);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);

    document.getElementById("table").appendChild(tr);
  }
}
