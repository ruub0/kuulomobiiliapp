var hidden = false;

  function year() {
    var time = document.getElementById('year');
    time.value = new Date().getFullYear();
  }
  year(); // Change year field to this year
   ////////////////////////////// Send Data /////////////////////////////////// -->
  function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // XHR for Chrome/Firefox/Opera/Safari.
      xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
      // XDomainRequest for IE.
      xhr = new XDomainRequest();
      xhr.open(method, url);
    } else {
      // CORS not supported.
      xhr = null;
    }
    return xhr;
  }
  function makeCorsRequest() {
    // This is a sample server that supports CORS.
    var url = "http://kuulo.lamk.fi/API/data";
    var xhr = createCORSRequest('POST', url);
    if (!xhr) {
      console.log('CORS not supported');
      return;
    }
    // Response handlers.
    xhr.onload = function() {
      console.log("xhr onload function");
      var text = xhr.status;  // gets status from server
      if (text == '200') {
          alert('Viesti lähetetty ');
          window.open("../www/index.html", "_top"); // Link back to homepage
      }
      else {
        alert('Viestin lähetys epäonnistui');
      }
    };
    xhr.onerror = function() {
      alert('Yhteys palvelimeen ei toimi.');
    };
    var data = {
        username:localStorage.getItem("userkey"),
        pw: localStorage.getItem("passkey"),
        kaynnin_tyyppi: document.getElementById('dropdownMenuButton').innerText,
        asiakas_lkm: parseInt(document.querySelector('#amount').value),
        tunnit: parseInt(document.querySelector('#time').value),
        vuosi:  parseInt(document.querySelector('#year').value),
        yli65v: parseInt(document.querySelector('#over65').value),
        alle65v: parseInt(document.querySelector('#under65').value)
    };

    var json = JSON.stringify(data);
      console.log(json);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(json);
  }
 ////////////////////////////End of send data///////////////////////////////////




                // Button name change -->
$(".dropdown-menu a").click(function(){
$("#dropdownMenuButton").html($(this).text()+'<span class="caret"></span>');
})
                //  <!-- Hide content -->
$(".dropdown-menu a").click(function(){
$('#collapseExample').collapse('hide');
});

                //change Modal styles
$('.modal-content').css("margin-top", $(window).height() / 3.5 - $('.modal-content').height() / 2);
$('.modal-header').css("backgroundColor", "#144580");
$('.modal-header').css("color", "#fff");
$('.modal-content').css("textAlign", "left");

function hideCustomer() {
    var x = document.getElementById("hidethis");
    x.style.display = "none";
    hidden = true;
};

function showCustomer() {
  var x = document.getElementById("hidethis");
  x.style.display = "block";
};

function getText() {  // Control what modal shows

  if (hidden == true) { // Calculate amount of customers from under and over 65
    document.querySelector('#amount').value = (parseInt(document.querySelector('#over65').value) + parseInt(document.querySelector('#under65').value));
  }
  if (document.getElementById('dropdownMenuButton').innerText != "Muut tilaisuudet") {

        var x = document.getElementById("ModalOver65");
        x.style.display = "none";
        var x = document.getElementById("ModalUnder65");
        x.style.display = "none";
        var x = document.getElementById("Asiakasmäärä");
        x.style.display = "block";
      }
    document.getElementById('Asiakasmäärä').innerText = "Asiakasmäärä: " +  document.querySelector('#amount').value
    document.getElementById('Kokonaisaika').innerText = "Kokonaisaika: " +  document.querySelector('#time').value
    document.getElementById('Lahipalvelu').innerText = "Lähipalvelun muoto: " +  document.getElementById('dropdownMenuButton').innerText
    document.getElementById('Asiakasmäärä').innerText = "Asiakasmäärä: " +  document.querySelector('#amount').value
    document.getElementById('Kokonaisaika').innerText = "Kokonaisaika: " +  document.querySelector('#time').value
    document.getElementById('Vuosi').innerText = "Vuosi: " +  document.querySelector('#year').value
    // Hide useless stuff and show over and under 65
  if (document.getElementById('dropdownMenuButton').innerText == "Muut tilaisuudet") {
        var x = document.getElementById("Asiakasmäärä");
        x.style.display = "none";
      document.getElementById('ModalOver65').innerText = "Yli 65-vuotiaat: " +  document.querySelector('#over65').value
      document.getElementById('ModalUnder65').innerText = "Alle 65-vuotiaat: " +  document.querySelector('#under65').value
  }
};
// Test if user has right inputs
$('#go').click(function() {
  var test1 = document.getElementById('dropdownMenuButton').innerText;

  if (test1 !== "Lähipalvelun muoto ") {
    $('#confirmModal').modal('show');
  }
  else {
      alert("Valitse haluamasi lähipalvelun muoto!")
  }
});
