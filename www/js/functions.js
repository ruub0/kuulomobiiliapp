var username;
var password;
console.log(username);
  //function year() {
  //  var time = document.getElementById('year');
  //  time.value = new Date().getFullYear();
//  }
//  year(); // Change year field to this year
   ////////////////////////////// Send Data /////////////////////////////////// -->
  function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // XHR for Chrome/Firefox/Opera/Safari.
      xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
      // XDomainRequest for IE.
      xhr = new XDomainRequest();
      xhr.open(method, url);
    } else {
      // CORS not supported.
      xhr = null;
    }
    return xhr;
  }
  function makeCorsRequest() {
    // This is a sample server that supports CORS.
    var url = "http://kuulo.lamk.fi:3000/data";
    var xhr = createCORSRequest('POST', url);
    if (!xhr) {
      alert('CORS not supported');
      return;
    }
    // Response handlers.
    xhr.onload = function() {
      console.log("xhr onload function");
      var text = xhr.status;  // gets status from server
      if (text == '200') {
          alert('Viesti lähetetty ');
          window.open("../www/index.html", "_top"); // Link back to homepage
      }
      else {
        alert('Viestin lähetys epäonnistui');
      }
    };
    xhr.onerror = function() {
      alert('Woops, there was an error making the request.');
    };
    var data = {
        username: "kuulo",
        pw: "12345",
        kaynnin_tyyppi: document.getElementById('dropdownMenuButton').innerText,
        asiakas_lkm: document.querySelector('#amount').value,
        tunnit: document.querySelector('#time').value,
        vuosi: document.querySelector('#year').value,
        yli65: document.querySelector('#over65').value,
        alle65: document.querySelector('#under65').value
    };
    var json = JSON.stringify(data);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(json);
  }
 ////////////////////////////End of send data///////////////////////////////////

////////////////////////////// Send Login /////////////////////////////////// -->
 function makeLoginRequest() {
  console.log("Näkyykö tiedot?");     // Checking if values are correct in browser console
  // This is a sample server that supports CORS.
  var url = "http://kuulo.lamk.fi:3000/user/login";
  var xhr = createCORSRequest('POST', url);
  if (!xhr) {
    console.log("Näkyykö tiedot1?");
    alert('CORS not supported');
    return;
  }
  // Response handlers.
  xhr.onload = function() {

    var text = xhr.status;  // gets status from server
    if (text == '200') {
      username = document.querySelector('#login').value;
      password = document.querySelector('#password').value;
        alert('Sisäänkirjautuminen onnistui ');
        window.open("../www/sendData.html", "_top"); // Link back to homepage
    }
    else {
      console.log("vituiks?");
      alert('Sisäänkirjautuminen epäonnistui');
    }
  };
  xhr.onerror = function() {
    alert('Woops, there was an error making the request.');
  };
  var data = {
    username: document.querySelector('#login').value,
    pw: document.querySelector('#password').value
  };
  var json = JSON.stringify(data);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(json);
 }
////////////////////////////End of send login///////////////////////////////////

                // Button name change -->
$(".dropdown-menu a").click(function(){
$("#dropdownMenuButton").html($(this).text()+' <span class="caret"></span>');
})
                //  <!-- Hide content -->
$(".dropdown-menu a").click(function(){
$('#collapseExample').collapse('hide');
});

function hideCustomer() {
    var x = document.getElementById("hidethis");
    x.style.display = "none";
    console.log("Piiloon?");
}

function showCustomer() {
  var x = document.getElementById("hidethis");
  x.style.display = "block";
  console.log("Näkyy?");
}
