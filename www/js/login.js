var username = localStorage.getItem("userkey");
var password = localStorage.getItem("passkey");
document.querySelector('#login').value = username;
document.querySelector('#password').value = password;

//change Modal styles
$('.modal-content').css("margin-top", $(window).height() / 3.5 - $('.modal-content').height() / 2);
$('.modal-header').css("backgroundColor", "#144580");
$('.modal-header').css("color", "#fff");
$('.modal-content').css("textAlign", "left");

////////////////////////////// Send Login /////////////////////////////////// -->
function createCORSRequest(method, url) {
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {
    // XHR for Chrome/Firefox/Opera/Safari.
    xhr.open(method, url, true);
  } else if (typeof XDomainRequest != "undefined") {
    // XDomainRequest for IE.
    xhr = new XDomainRequest();
    xhr.open(method, url);
  } else {
    // CORS not supported.
    xhr = null;
  }
  return xhr;
}
 function makeLoginRequest() {
  // This is a sample server that supports CORS.
  var url = "http://kuulo.lamk.fi/API/user/login";
  var xhr = createCORSRequest('POST', url);
  if (!xhr) {
    console.log('CORS not supported');
    return;
  }
  // Response handlers.
  xhr.onload = function() {
    var text = xhr.status;  // gets status from server
    if (text == '200') {
      var username = document.querySelector('#login').value;
      localStorage.setItem("userkey", username);

      var password = document.querySelector('#password').value;
      localStorage.setItem("passkey", password);

        // console.log('Sisäänkirjautuminen onnistui');
        window.open("../www/sendData.html", "_top"); // Link back to homepage
    }
    else {
      console.log('Sisäänkirjautuminen epäonnistui');
    }
  };
  xhr.onerror = function() {
    console.log('Woops, there was an error making the request.');
  };
  var data = {
    username: document.querySelector('#login').value,
    pw: document.querySelector('#password').value
  };
  var json = JSON.stringify(data);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(json);
 }
////////////////////////////End of send login///////////////////////////////////
//////Enter sends login //////////////
var input = document.getElementById("password");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById("myBtn").click();
  }
});
